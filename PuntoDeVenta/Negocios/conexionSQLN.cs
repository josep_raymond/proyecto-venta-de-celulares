﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;

namespace Negocios
{
    public class conexionSQLN
    {
        ConexionSQL cn = new ConexionSQL();

        /*user contrasena*/
        public int conSQL(string usuario, string contrasena)
        {
            return cn.consultalogin(usuario, contrasena);

        }


        public DataTable ConsultaDT()
        {
            return cn.ConsultaUsuariosDG();
        }

        public DataTable ConsultaInventariooDT()
        {
            return cn.ConsultaInventariooDG();
        }


        // =========================METODOS FORMUSUARIO=============================
        public int InsertarUsuario(string nom, string apel, string dni, string tel, string usuario, string contraseña)
        {


            return cn.InsertarUsuario(nom,apel,dni,tel,usuario,contraseña);
        }

        public int ModificarUsuario(string nom, string apel, string dni, string tel, string usuario, string contraseña)
        {
           

            return cn.ModificarUsuario(nom, apel, dni, tel, usuario, contraseña);
        }


        public int EliminarUsuario(string dni)
        {
            int flag = 0;
 
            return cn.EliminarUsuario(dni);
        }
        // =========================METODOS INVENTARIO=============================

        public int AgregarProducto(string producto, string categoria, string precio, string cantidad, string codigo)
        {


            return cn.AgregarProducto(producto, categoria, precio, cantidad, codigo);
        }

        public int EditarProducto(string producto, string categoria, string precio, string cantidad, string codigo)
        {


            return cn.EditarProducto(producto, categoria, precio, cantidad, codigo);
        }

        public int EliminarProducto(string codigo)
        {
            int flag = 0;

            return cn.EliminarProducto(codigo);

        }

    }
}
 