﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocios;

namespace Presentacion

{
    public partial class FormUsuario : Form
    {
        conexionSQLN cn = new conexionSQLN();
        public FormUsuario()
        {
            InitializeComponent();
            dataGridView1.DataSource = cn.ConsultaDT();    

        }

        private void FormUsuario_Load(object sender, EventArgs e)
        {

        }

        //CERRAR
        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //BOTON DE NUEVO USUARIO
        private void button1_Click(object sender, EventArgs e)
        {
            //(nom, apel, dni, tel, usuario, contraseña)
            cn.InsertarUsuario(txt_nombre.Text, txt_apellidos.Text, txt_dni.Text, txt_telefono.Text, txt_usuario.Text, txt_contrasena.Text);
           dataGridView1.DataSource = cn.ConsultaDT();

        }
        //BOTON DE MODIFICAR USUARIO
        private void button2_Click(object sender, EventArgs e)
        {
           cn.ModificarUsuario(txt_nombre.Text, txt_apellidos.Text, txt_dni.Text, txt_telefono.Text, txt_usuario.Text, txt_contrasena.Text);
          
            dataGridView1.DataSource = cn.ConsultaDT();
        }

        //BOTON DE ELIMINAR USUARIO
        private void button3_Click(object sender, EventArgs e)
        {
            cn.EliminarUsuario(txt_dni.Text);
            dataGridView1.DataSource = cn.ConsultaDT();
        }


    }
}
