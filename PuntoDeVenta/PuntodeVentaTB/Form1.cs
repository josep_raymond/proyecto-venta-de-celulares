﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocios;


namespace PuntodeVentaTB
{
        public partial class Form1 : Form
    {
        conexionSQLN cn = new conexionSQLN();
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_entrar_Click(object sender, EventArgs e)
        {
            if (cn.conSQL(txt_usuario.Text, txt_contrasena.Text) == 1) 
            {
                MessageBox.Show("El usuario ha sido encontrado");

                this.Hide();

                VentanaPrincipal v1 = new VentanaPrincipal();
                v1.Show();

            }
            else
            {
                MessageBox.Show("El usuario No ha sido encontrado");
            }
        }
    }
}
