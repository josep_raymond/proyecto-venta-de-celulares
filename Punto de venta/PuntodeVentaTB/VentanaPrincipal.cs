﻿using Presentacion;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace PuntodeVentaTB
{
    public partial class VentanaPrincipal : Form
    {

        private DataTable dt;


        public VentanaPrincipal()
        {
            InitializeComponent();

            txt_ImpVenta1.Text = txt_impVentaedit.Text;
            txt_Desc1.Text = txt_DescEdit.Text;


            dt = new DataTable();
            dt.Columns.Add("Codigo");
            dt.Columns.Add("Producto");
            dt.Columns.Add("Precio x Unidad");
            dt.Columns.Add("Cantidad");
            dt.Columns.Add("Descuento ");
            dt.Columns.Add("Precio Total");

            dtg_Facturacion.DataSource = dt;


        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();         
        }

        //BOTON USUARIOS
        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            
            FormUsuario v1 = new FormUsuario();
            this.Hide();
            v1.ShowDialog();
            this.Show();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        //BOTON INVENTARIO
        private void toolStripMenuItem3_Click_1(object sender, EventArgs e)
        {
            Inventario v1 = new Inventario();
            this.Hide();
            v1.ShowDialog();
            this.Show();
        }

        private void VentanaPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void btn_AgregarProducto_Click(object sender, EventArgs e)
        {
            DataRow row = dt.NewRow();

            row["Codigo"] = txt_CodigoProducto.Text;
            row["Producto"] = "Producto X";
            row["Precio x Unidad"] = "3500";
            row["Cantidad"] = txt_Cantidad.Text;
            row["Descuento"] = txt_Desc1.Text;
            row["Precio TOTAL"] = "70000";

            dt.Rows.Add(row);
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }
    }
}
