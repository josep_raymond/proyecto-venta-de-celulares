﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocios;

namespace PuntodeVentaTB
{
    public partial class Inventario : Form
    {
        conexionSQLN cn = new conexionSQLN();
        public Inventario()
        {
            InitializeComponent();
            dataGridView1.DataSource = cn.ConsultaInventariooDT();
        }

       //BOTON CERRAR
        private void btn_cerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //BOTON ELIMINAR
         private void button3_Click(object sender, EventArgs e)
        
        {
            cn.EliminarProducto(txt_codigo.Text);
            dataGridView1.DataSource = cn.ConsultaInventariooDT();
        }





        //BOTON EDITAR
        private void btn_editar_Click(object sender, EventArgs e)
        {
            cn.EditarProducto(txt_producto.Text, txt_categoria.Text, txt_precio.Text, txt_cantidad.Text, txt_codigo.Text);
            dataGridView1.DataSource = cn.ConsultaInventariooDT();
        }

        //BOTON AGREGAR
        private void btn_agregar_Click(object sender, EventArgs e)
        {
            //(producto, categoria, precio, cantidad, codigo, contraseña)
            cn.AgregarProducto(txt_producto.Text, txt_categoria.Text, txt_precio.Text, txt_cantidad.Text, txt_codigo.Text);
            dataGridView1.DataSource = cn.ConsultaInventariooDT();
        }
    }
}
