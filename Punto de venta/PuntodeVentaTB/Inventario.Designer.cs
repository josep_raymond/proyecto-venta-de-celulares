﻿
namespace PuntodeVentaTB
{
    partial class Inventario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btn_cerrar = new System.Windows.Forms.Button();
            this.btn_agregar = new System.Windows.Forms.Button();
            this.btn_editar = new System.Windows.Forms.Button();
            this.btn_eliminar = new System.Windows.Forms.Button();
            this.txt_precio = new System.Windows.Forms.TextBox();
            this.txt_categoria = new System.Windows.Forms.TextBox();
            this.txt_producto = new System.Windows.Forms.TextBox();
            this.txt_cantidad = new System.Windows.Forms.TextBox();
            this.txt_codigo = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btn_nomProducto = new System.Windows.Forms.Button();
            this.btn_categoria = new System.Windows.Forms.Button();
            this.btn_precio = new System.Windows.Forms.Button();
            this.btn_cantidad = new System.Windows.Forms.Button();
            this.btn_codigo = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(605, 259);
            this.dataGridView1.TabIndex = 0;
            // 
            // btn_cerrar
            // 
            this.btn_cerrar.Location = new System.Drawing.Point(674, 378);
            this.btn_cerrar.Name = "btn_cerrar";
            this.btn_cerrar.Size = new System.Drawing.Size(75, 23);
            this.btn_cerrar.TabIndex = 1;
            this.btn_cerrar.Text = "Cerrar";
            this.btn_cerrar.UseVisualStyleBackColor = true;
            this.btn_cerrar.Click += new System.EventHandler(this.btn_cerrar_Click);
            // 
            // btn_agregar
            // 
            this.btn_agregar.Location = new System.Drawing.Point(35, 378);
            this.btn_agregar.Name = "btn_agregar";
            this.btn_agregar.Size = new System.Drawing.Size(75, 23);
            this.btn_agregar.TabIndex = 2;
            this.btn_agregar.Text = "Agregar";
            this.btn_agregar.UseVisualStyleBackColor = true;
            this.btn_agregar.Click += new System.EventHandler(this.btn_agregar_Click);
            // 
            // btn_editar
            // 
            this.btn_editar.Location = new System.Drawing.Point(141, 378);
            this.btn_editar.Name = "btn_editar";
            this.btn_editar.Size = new System.Drawing.Size(75, 23);
            this.btn_editar.TabIndex = 3;
            this.btn_editar.Text = "Editar";
            this.btn_editar.UseVisualStyleBackColor = true;
            this.btn_editar.Click += new System.EventHandler(this.btn_editar_Click);
            // 
            // btn_eliminar
            // 
            this.btn_eliminar.Location = new System.Drawing.Point(247, 378);
            this.btn_eliminar.Name = "btn_eliminar";
            this.btn_eliminar.Size = new System.Drawing.Size(75, 23);
            this.btn_eliminar.TabIndex = 4;
            this.btn_eliminar.Text = "Eliminar";
            this.btn_eliminar.UseVisualStyleBackColor = true;
            this.btn_eliminar.Click += new System.EventHandler(this.button3_Click);
            // 
            // txt_precio
            // 
            this.txt_precio.Location = new System.Drawing.Point(247, 316);
            this.txt_precio.Name = "txt_precio";
            this.txt_precio.Size = new System.Drawing.Size(100, 20);
            this.txt_precio.TabIndex = 5;
            // 
            // txt_categoria
            // 
            this.txt_categoria.Location = new System.Drawing.Point(141, 316);
            this.txt_categoria.Name = "txt_categoria";
            this.txt_categoria.Size = new System.Drawing.Size(100, 20);
            this.txt_categoria.TabIndex = 6;
            // 
            // txt_producto
            // 
            this.txt_producto.Location = new System.Drawing.Point(35, 316);
            this.txt_producto.Name = "txt_producto";
            this.txt_producto.Size = new System.Drawing.Size(100, 20);
            this.txt_producto.TabIndex = 7;
            // 
            // txt_cantidad
            // 
            this.txt_cantidad.Location = new System.Drawing.Point(353, 316);
            this.txt_cantidad.Name = "txt_cantidad";
            this.txt_cantidad.Size = new System.Drawing.Size(100, 20);
            this.txt_cantidad.TabIndex = 8;
            // 
            // txt_codigo
            // 
            this.txt_codigo.Location = new System.Drawing.Point(459, 316);
            this.txt_codigo.Name = "txt_codigo";
            this.txt_codigo.Size = new System.Drawing.Size(100, 20);
            this.txt_codigo.TabIndex = 9;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(632, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 50);
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // btn_nomProducto
            // 
            this.btn_nomProducto.Location = new System.Drawing.Point(35, 287);
            this.btn_nomProducto.Name = "btn_nomProducto";
            this.btn_nomProducto.Size = new System.Drawing.Size(75, 23);
            this.btn_nomProducto.TabIndex = 11;
            this.btn_nomProducto.Text = "Producto";
            this.btn_nomProducto.UseVisualStyleBackColor = true;
            // 
            // btn_categoria
            // 
            this.btn_categoria.Location = new System.Drawing.Point(141, 287);
            this.btn_categoria.Name = "btn_categoria";
            this.btn_categoria.Size = new System.Drawing.Size(75, 23);
            this.btn_categoria.TabIndex = 12;
            this.btn_categoria.Text = "Categoria";
            this.btn_categoria.UseVisualStyleBackColor = true;
            // 
            // btn_precio
            // 
            this.btn_precio.Location = new System.Drawing.Point(247, 287);
            this.btn_precio.Name = "btn_precio";
            this.btn_precio.Size = new System.Drawing.Size(75, 23);
            this.btn_precio.TabIndex = 13;
            this.btn_precio.Text = "Precio";
            this.btn_precio.UseVisualStyleBackColor = true;
            // 
            // btn_cantidad
            // 
            this.btn_cantidad.Location = new System.Drawing.Point(353, 287);
            this.btn_cantidad.Name = "btn_cantidad";
            this.btn_cantidad.Size = new System.Drawing.Size(75, 23);
            this.btn_cantidad.TabIndex = 14;
            this.btn_cantidad.Text = "Cantidad";
            this.btn_cantidad.UseVisualStyleBackColor = true;
            // 
            // btn_codigo
            // 
            this.btn_codigo.Location = new System.Drawing.Point(459, 287);
            this.btn_codigo.Name = "btn_codigo";
            this.btn_codigo.Size = new System.Drawing.Size(75, 23);
            this.btn_codigo.TabIndex = 15;
            this.btn_codigo.Text = "Codigo";
            this.btn_codigo.UseVisualStyleBackColor = true;
            // 
            // Inventario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_codigo);
            this.Controls.Add(this.btn_cantidad);
            this.Controls.Add(this.btn_precio);
            this.Controls.Add(this.btn_categoria);
            this.Controls.Add(this.btn_nomProducto);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txt_codigo);
            this.Controls.Add(this.txt_cantidad);
            this.Controls.Add(this.txt_producto);
            this.Controls.Add(this.txt_categoria);
            this.Controls.Add(this.txt_precio);
            this.Controls.Add(this.btn_eliminar);
            this.Controls.Add(this.btn_editar);
            this.Controls.Add(this.btn_agregar);
            this.Controls.Add(this.btn_cerrar);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Inventario";
            this.Text = "Inventario";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_cerrar;
        private System.Windows.Forms.Button btn_agregar;
        private System.Windows.Forms.Button btn_editar;
        private System.Windows.Forms.Button btn_eliminar;
        private System.Windows.Forms.TextBox txt_precio;
        private System.Windows.Forms.TextBox txt_categoria;
        private System.Windows.Forms.TextBox txt_producto;
        private System.Windows.Forms.TextBox txt_cantidad;
        private System.Windows.Forms.TextBox txt_codigo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btn_nomProducto;
        private System.Windows.Forms.Button btn_categoria;
        private System.Windows.Forms.Button btn_precio;
        private System.Windows.Forms.Button btn_cantidad;
        private System.Windows.Forms.Button btn_codigo;
    }
}